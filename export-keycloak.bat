docker exec -it irc-keycloak-with-themes opt/jboss/keycloak/bin/standalone.sh ^
-Djboss.socket.binding.port-offset=100 ^
-Dkeycloak.migration.action=export ^
-Dkeycloak.migration.provider=singleFile ^
-Dkeycloak.migration.realmName=LMS ^
-Dkeycloak.migration.usersExportStrategy=REALM_FILE ^
-Dkeycloak.migration.file=/tmp/lms-export/LMS.json