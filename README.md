# Keycloak with "custom" Themes

To start your development environment:

## Prerequisites
- [Access to Keycloak Theme Development Documentation](https://www.keycloak.org/docs/latest/server_development/index.html#_themes)
- Latest Docker installed
- Docker Compose installed


#

## Launching the environment

1. Make sure our start and stop scripts are executable (windows machines will ned a bash interpter such a [cygwin](https://www.cygwin.com/)

    ```chmod +x *.sh```

2. Make sure you have Docker running. The following should show a process table wtihout any errors

    ```docker ps```

3. Now we simply type the following to spin up our Dev Stack

    ```./start.sh```

4. To work within Keycloak visit the it at @ http://localhost:8888

5. The first thing you need to do is make sure your realms theme is set to ```custom``` [this is the generic name we give our customized themes]

#

## How to navigate to the various pages

[Login Page](http://localhost:8888/auth/realms/LMS/protocol/openid-connect/auth?client_id=security-admin-console&redirect_uri=http%3A%2F%2Flocalhost%3A8888%2Fauth%2Fadmin%2FLMS%2Fconsole%2F&state=fcbb17c3-0761-40be-8f3d-c7552941edef&response_mode=fragment&response_type=code&scope=openid&nonce=ab672532-fc1d-48be-8748-8e662fc62176&code_challenge=ZQKQQy0F7tyv9uOH_1QsLbawy8RxOPXXEXdfAVmyVdI&code_challenge_method=S256)

[Forgot Password](http://localhost:8888/auth/realms/LMS/login-actions/reset-credentials?client_id=security-admin-console&tab_id=dV43NayKKIg)

[Security Admin](http://localhost:8888/auth/admin/LMS/console/#/realms/cwa_cms/users)

[Account Management](http://localhost:8888/auth/realms/LMS/account?referrer=security-admin-console&referrer_uri=http%3A%2F%2Flocalhost%3A8888%2Fauth%2Fadmin%2FLMS%2Fconsole%2F%23%2Frealms%2FLMS%2Fusers)

