#!/bin/sh
# Check if docker is running
docker_state=$(docker info >/dev/null 2>&1)
if [[ $? -ne 0 ]]; then
    echo "Docker does not seem to be running, start the docker daemon first and retry"
    exit 1
fi
docker-compose down;
docker-compose up -d